import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'pnp-common-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input() logoUrl: string;
  @Input() titleForm: string;
  @Input() infoForm: string;
  @Input() infoFooterForm: string;
  /**
   * Event handler login reveice value login
   */
  @Output() onSubmit = new EventEmitter();

  loginForm: FormGroup;
  display;


  constructor() {
  }

  ngOnInit() {
    this.display = 'none';
    this.loginForm = new FormGroup(
      {
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', Validators.required),
        remember: new FormControl(false),
      }
    );
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  onLogin() {
    if (this.loginForm.valid) {
      this.onSubmit.emit(this.loginForm.value);
      this.onOpenHandled();
    }
  }

  onCloseHandled() {
    this.display = 'none';
  }

  onOpenHandled() {
    this.display = 'block';
  }

}
