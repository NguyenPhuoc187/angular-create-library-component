import {Component, Input} from '@angular/core';
import {IMenuLeftSideBar} from './left-sidebar';

@Component({
  selector: 'pnp-common-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss']
})
export class LeftSidebarComponent {
  @Input() dataSource: Array<IMenuLeftSideBar>;
  @Input() sideBarStyle: string;
}
