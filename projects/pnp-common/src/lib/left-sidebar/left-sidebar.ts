export interface IMenuLeftSideBar {
  key: string;
  name: string;
  link: string;
  icon: string;
  subMenu?: Array<IMenuLeftSideBar>;
}
