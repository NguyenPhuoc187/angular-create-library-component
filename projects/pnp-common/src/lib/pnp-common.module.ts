import {NgModule} from '@angular/core';
import {FooterComponent} from './footer/footer.component';
import {HeaderComponent} from './header/header.component';
import {LeftSidebarComponent} from './left-sidebar/left-sidebar.component';
import {LoginComponent} from './login/login.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [FooterComponent,
    HeaderComponent,
    LeftSidebarComponent, LoginComponent],
  imports: [CommonModule, ReactiveFormsModule, RouterModule],
  exports: [
    FooterComponent,
    HeaderComponent,
    LeftSidebarComponent,
    LoginComponent]
})
export class PnpCommonModule {
}
