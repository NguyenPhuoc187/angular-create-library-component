import {Component, Input} from '@angular/core';
import {IMenu, IUser} from './header';

@Component({
  selector: 'pnp-common-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  // LOGO

  // đường dẫn của image logo
  @Input() logoUrl: string;
  // chiều ngang của logo
  @Input() logoWidth: number | string;
  // link href for Logo
  @Input() hrefLogo: string;
  // dữ liệu của menu
  @Input() dataMenu: Array<IMenu>;
  // dữ liệu của user
  @Input() dataUser: IUser;
}
