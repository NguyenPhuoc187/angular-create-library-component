export interface IMenu {
  key?: string;
  name: string;
  linkUrl: string;
}

export interface IUser {
  userName: string;
  profile: string;
  info: string;
  displayName: string;
  avatarUrl: string;
}
