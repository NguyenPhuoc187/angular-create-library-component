# Ng7 Template Common

* A simple library test provides 4 components including header, login, left sidebar, footer.
* This library is generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## Install Library

- Install the **ng7-template-common** npm packages:

  `npm install --save ng7-template-common`

- Install the **bootstrap**, **jquery**, **popper.js** npm packages:

`npm install --save bootstrap jquery popper.js`

#### <a name="additional-configuration"></a>Additional Configuration ####
* [Configuring Angular CLI]
Go to **angular.json** and add references to the necessary files:

```
{
  ...
  "projects": {
    "ProjectName": {
      ...
      "architect": {
        "build": {
          ...
          "options": {
            ...
            "styles": [
              "node_modules/bootstrap/dist/css/bootstrap.css",
              "src/styles.scss"
            ],
            "scripts": [
              "node_modules/jquery/dist/jquery.js",
              "node_modules/popper.js/dist/umd/popper.js",
              "node_modules/bootstrap/dist/js/bootstrap.js"
            ],
            ...
          },
          ...
        },
        ...
      }
    },
    ...
  },
  ...
}
```

#### <a name="import-modules"></a>Import Ng7TemplateCommon Modules ####

Go to your main .ts file (usually *src/app.module.ts*) and import the required modules to your app:

```
...
import { Ng7TemplateCommonModule } from 'ng7-template-common';

@NgModule({
    ...
    imports: [
        ...
        Ng7TemplateCommonModule,
        ...
    ]
})
export class AppModule {}
```

## Library's components
- `<ng7-template-common-login>` used for form login and submit data view popup
- `<ng7-template-common-header>` used to display menu,logo,avatar, profile
- `<ng7-template-common-left-sidebar>` used to display list menu in left sidebar
- `<ng7-template-common-footer>` used to display static info footer


#### Example Components  ####

Now you can use a Ng7TemplateCommon Login component in your application:

```
@Component({
    selector: 'my-app',
    template: '
    <ng7-template-common-login 
      logoUrl="http://www.awrania.com/images/bootstrap_logo.png"
      titleForm="Sign in"
      infoForm="Library Form Login"
      infoFooterForm="&copy; 2019" 
      (onSubmit)="onLogin($event)"
    >
    </ng7-template-common-login>'
})
export class AppComponent {
    onLogin(data) {
        console.log('Username: ',data.username);
    }
}
```

## API

  ### Header Component  ####

#### @Inputs()

| Input            | Type         | Required                   | Description                                                                 |
| ---------------- | -------      | -------------------------- | ----------------------------------------------------------------------------|
| logoUrl          | string       | **YES**                    | Image path of the logo                                                      |
| logoWidth        | number       | Optional, default: 45      | Set width of logo image.                                                    |
| hrefLogo         | string       | Optional, default: ''      | The link points to the homepage of that Logo. Ex: `http://localhost:3000/`  |
| nameNavItemExpr  | string       | **YES**                    | Specifies the data field whose values should be displayed **Name**.         |
| linkNavItemExpr  | string       | **YES**                    | Specifies the data field whose values should be displayed **Link**          |
| dataSourceMenu   | Array<Object>| **YES**                    | Binds the widget to data. Ex: [{name:'Home',link:'/home'}]                  |
| avatarUrl        | string       | **YES**                    | Path image of avatar user.                                                  |
| userName         | string       | **YES**                    | Specifies the Username to be displayed on the navbar.                       |
| displayName      | string       | Optional, default: ''      | Specifies the Full name to be displayed on the dropdown profile.            |
| profile          | string       | Optional, default: ''      | Specifies the Profile.                                                      |
| subInfo          | string       | Optional, default: ''      | Specifies the info.                                                         |


#### Usage

Now you can use a Header Component in your application:

 ```
 import { Component, OnInit } from '@angular/core';
@Component({
     selector: 'my-app',
     template: `
     <ng7-template-common-header
         logoUrl="http://www.awrania.com/images/bootstrap_logo.png"
         hrefLogo="/"
         logoWidth="40"
         nameNavItemExpr="name"
         linkNavItemExpr="linkUrl"
         avatarUrl="https://cdn4.iconfinder.com/data/icons/people-avatars-12/24/people_avatar_head_iron_man_marvel_hero-512.png"
         [displayName]="dataUser.displayName"
         [userName]="dataUser.userName"
         [profile]="dataUser.profile"
         [subInfo]="dataUser.info"
         [dataSourceMenu]="dataMenu"
     >
     </ng7-template-common-header>`
})
export class AppComponent implements OnInit {
    dataMenu: Array<any>;
    dataUser: any;
    constructor() {}
    ngOnInit(): void {
      this.dataMenu = [
        {name: 'Home', linkUrl: '/'},
        {name: 'About', linkUrl: '/about'},
      ];
      this.dataUser = {
            userName: 'KennyPham',
            displayName: 'Kenny Pham',
            profile: 'Web Developer',
            info: '3 year development website',
            avatarUrl: 'https://cdn4.iconfinder.com/data/icons/people-avatars-12/24/people_avatar_head_iron_man_marvel_hero-512.png'
      };
    }
}
 ```


### Left Sidebar Component  ####

#### @Inputs()


| Input            | Type         | Required                   | Description                                                                                                                                |
| ---------------- | -------      | -------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------|
| keyExpr          | string       | **YES**                    | Specifies which data field provides keys for data items.                                                                                   |
| nameExpr         | string       | **YES**                    | Specifies the data field whose values should be displayed **Name**.                                                                        |
| linkExpr         | string       | **YES**                    | Specifies the data field whose values should be displayed **Link**                                                                         |
| iconExpr         | string       | Optional                   | Specifies the data field whose values should be displayed **Icon**                                                                         |
| itemsExpr        | string       | Optional                   | Specifies the data field whose values should be displayed **Sub Item**                                                                     |
| dataSource       | Array<Object>| **YES**                    | Binds the widget to data. Ex: [{key:'about',name:'About us', link:'/about-us', items:[{key:'one',name:'About one', link:'/about-one'}]}]   |
                                                                   
#### Usage and Example ####

Now you can use a Left Sidebar Component in your application:

```
@Component({
    selector: 'my-app',
    template: `
    <ng7-template-common-left-sidebar
            keyExpr="key"
            nameExpr="name"
            linkExpr="link"
            iconExpr="icon"
            itemsExpr="items"
            [dataSource]="dataSource"
          ></ng7-template-common-left-sidebar>`
})
export class AppComponent implements OnInit {
    dataMenu: Array<any>;
    dataUser: any;
    constructor() {}
    ngOnInit(): void {
      this.dataSource = [
        {
          key: 'home',
          name: 'Home Page',
          link: '/home',
          icon: '',
          items: [
            {key: 'dashboard1', name: 'Dashboard v1', link: '/dashboard-v1'},
            {key: 'dashboard2', name: 'Dashboard v2', link: '/dashboard-v2'}
          ]
        },
        {
          key: 'contact',
          name: 'Contact',
          link: '/contact',
          icon: '',
        }
      ];
    }
}
```


### Login Component  ####

#### @Inputs()

| Input            | Type         | Required                   | Description                                                                                                                                |
| ---------------- | -------      | -------------------------- | -------------------------------------|
| logoUrl          | string       | Optional                   | Image path of the logo               |
| titleForm        | string       | Optional                   | Title of the login form              |
| infoForm         | string       | Optional                   | Information of login form            |
| infoFooterForm   | string       | Optional                   | Footer information of login form     |

#### @Outputs()

| Output           | Type       | Required                                 | Description                                                                                                           
| ---------------- | ---------- | ---------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| onSubmit         | any        | Please enter your username and password  | A function that is executed when a form submit and send data. Ex Data received: {username: 'Kenny', password: '123456', remember: true}  |

#### Usage and Example ####

```
@Component({
    selector: 'my-app',
    template: '
    <ng7-template-common-login 
      logoUrl="http://www.awrania.com/images/bootstrap_logo.png"
      titleForm="Sign in"
      infoForm="Library Form Login"
      infoFooterForm="&copy; 2019" 
      (onSubmit)="onLogin($event)"
    >
    </ng7-template-common-login>'
})
export class AppComponent {
    onLogin(data) {
        console.log('Username: ',data.username);
    }
}
```

### Footer Component  ####

Enter static content as the example below

#### Usage and Example ####

```
@Component({
    selector: 'my-app',
    template: '
    <ng7-template-common-footer>&copy; Copyright 2019</ng7-template-common-footer>
    '
})
export class AppComponent {}
```

## <a name="license"></a>License ##

**Copyright (c) 2019 Kenny Pham*

## <a name="version-history"></a>Version history ###

| Ng7TemplateCommon | Angular |
| ---------- | ----------------|
| v1.0+ | v7.0+ |
