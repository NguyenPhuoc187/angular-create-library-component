import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'ng7-template-common-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // đường dẫn hình ảnh của logo form login
  @Input() logoUrl: string;

  // tiêu đề
  @Input() titleForm: string;

  // thông tin thêm hiển thị phía dưới tiêu đề
  @Input() infoForm: string;

  // thông tin footer của login
  @Input() infoFooterForm: string;

  /**
   * Event handler login reveice value login
   */
  @Output() onSubmit = new EventEmitter();

  loginForm: FormGroup;
  display: string;

  constructor() {
  }

  ngOnInit() {
    this.display = 'none';
    this.loginForm = new FormGroup(
      {
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', Validators.required),
        remember: new FormControl(false),
      }
    );
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  get remember() {
    return this.loginForm.get('remember');
  }

  onLogin() {
    if (this.loginForm.valid) {
      this.onSubmit.emit(this.loginForm.value);
      this.onOpenHandled();
    }
  }

  onCloseHandled() {
    this.display = 'none';
  }

  onOpenHandled() {
    this.display = 'block';
  }

}
