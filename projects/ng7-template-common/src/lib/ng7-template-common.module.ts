import {NgModule} from '@angular/core';
import {FooterComponent} from './footer/footer.component';
import {HeaderComponent} from './header/header.component';
import {LoginComponent} from './login/login.component';
import {LeftSidebarComponent} from './left-sidebar/left-sidebar.component';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [FooterComponent, HeaderComponent, LoginComponent, LeftSidebarComponent],
  imports: [
    CommonModule, ReactiveFormsModule, RouterModule
  ],
  exports: [FooterComponent, HeaderComponent, LoginComponent, LeftSidebarComponent]
})
export class Ng7TemplateCommonModule {
}
