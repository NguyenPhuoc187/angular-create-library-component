import {Component, Input} from '@angular/core';


@Component({
  selector: 'ng7-template-common-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss']
})
export class LeftSidebarComponent {
  // chỉ định từ khóa trong mảng dữ liệu cung cấp giá trị key
  @Input() keyExpr: string;

  // chỉ định từ khóa trong mảng dữ liệu cung cấp giá trị name
  @Input() nameExpr: string;

  // chỉ định từ khóa trong mảng dữ liệu cung cấp giá trị link
  @Input() linkExpr: string;

  // chỉ định từ khóa trong mảng dữ liệu cung cấp giá trị icon
  @Input() iconExpr: string;

  // chỉ định từ khóa trong mảng dữ liệu cung cấp danh sách menu con
  @Input() itemsExpr: string;

  // mảng dữ liệu danh sách menu
  @Input() dataSource: Array<any>;

  @Input() sideBarStyle: string;
}
