import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ng7-template-common-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  // LOGO

  // đường dẫn của image logo
  @Input() logoUrl: string;
  // chiều ngang của image logo
  @Input() logoWidth: number;
  // link href for Logo
  @Input() hrefLogo: string;

  // chỉ định từ khóa nào trong mảng dữ liệu cung cấp cho tên của menu
  @Input() nameNavItemExpr: string;
  // chỉ định từ khóa nào trong mảng dữ liệu cung cấp cho giá trị link của menu
  @Input() linkNavItemExpr: string;
  // dữ liệu của menu
  @Input() dataSourceMenu: Array<any>;

  // đường dẫn hình ảnh của User
  @Input() avatarUrl: string;

  // show display name
  @Input() displayName: string;
  // show profile
  @Input() profile: string;
  // show sub info
  @Input() subInfo: string;
  // User Name
  @Input() userName: string;

  ngOnInit(): void {
    this.logoWidth = (this.logoWidth === undefined) ? 45 : this.logoWidth;
  }
}
