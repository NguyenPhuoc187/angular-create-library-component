/*
 * Public API Surface of ng7-template-common
 */

export * from './lib/ng7-template-common.module';

// component
export * from './lib/footer/footer.component';
export * from './lib/header/header.component';
export * from './lib/login/login.component';
export * from './lib/left-sidebar/left-sidebar.component';
