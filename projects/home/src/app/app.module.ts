import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Ng7TemplateCommonModule} from 'ng7-template-common';





@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng7TemplateCommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
