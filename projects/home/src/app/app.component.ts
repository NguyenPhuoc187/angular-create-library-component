import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'home-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'home';
  dataMenu: Array<any>;
  dataUser: any;
  dataSource: Array<any>;

  constructor() {

  }

  ngOnInit(): void {
    this.dataMenu = [
      {name: 'Home', linkUrl: '/'},
      {name: 'About', linkUrl: '/about'},
    ];

    this.dataUser = {
      userName: 'KennyPham',
      displayName: 'Kenny Pham',
      profile: 'Web Developer',
      info: '3 year development website',
      avatarUrl: 'https://cdn4.iconfinder.com/data/icons/people-avatars-12/24/people_avatar_head_iron_man_marvel_hero-512.png'
    };

    this.dataSource = [
      {
        key: 'home',
        name: 'Trang Chủ',
        link: '/home',
        icon: '',
        subMenu: [
          {
            key: 'about', name: 'Giới thiệu', link: '/about'
          },
          {
            key: 'aboutus', name: 'Giới thiệu 2', link: '/about2'
          }
        ]
      },
      {
        key: 'contact',
        name: 'Liên hệ',
        link: '/contact',
        icon: '',
      }
    ];
  }

  onSubmit(value: any) {
    console.log(value);
  }

  // todo: cần fix lại giao diện của header khi hover vào link - chỉnh màu theo trang ngbootstrap
  // todo: cần hoàn chỉnh xong phần readme để cấu hình cài đặt, yêu cầu thư viện, hướng dẫn sử dụng các component
}
